---
layout: handbook-page-toc
title: "GitLab Special Programs: EDU, OSS and GitLab for Startups"
category: License and subscription
description: Redirecting EDU, OSS or Startups subscription inquiries.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

##### Overview

We do not offer any non-profit discounts or programs, but users can apply for our special programs if they meet the requirements.

Use the relevant workflow below when there's a ticket about [GitLab for Education](https://about.gitlab.com/solutions/education/), [GitLab for Open Source](https://about.gitlab.com/solutions/open-source/) or [GitLab for Startups](https://about.gitlab.com/solutions/startups/).

Note that our special programs do not include support, but support can be purchased additionally for the Open Source and Startups programs. Internal escalations for all three of the programs can be made via Slack channel [#community-programs](https://join.slack.com/share/zt-op8hxhoy-V4TBiVh_r41H6uelJeCPfA).

---

##### GitLab for Education (EDU) Workflow

1. When a customer is looking to apply or renew their existing EDU subscription, send the [`General::EDU Response`](https://gitlab.com/gitlab-com/support/support-ops/zendesk-global/macros/-/blob/master/macros/active/General/EDU%20Response.yaml) macro.

##### GitLab for Open Source (OSS) Workflow

1. When a customer is looking to apply or renew their existing OSS subscription, send the [`General::OSS Response`](https://gitlab.com/gitlab-com/support/support-ops/zendesk-global/macros/-/blob/master/macros/active/General/OSS%20Response.yaml) macro.

##### GitLab for Startups Workflow

1. When a customer is looking to apply or renew their existing Startup subscription, send the [`General::Startup Response`](https://gitlab.com/gitlab-com/support/support-ops/zendesk-global/macros/-/blob/master/macros/active/General/Startup%20Response.yaml) macro.
